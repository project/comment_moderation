<?php

/**
 * @file comment.tpl.php
 * Default theme implementation for comments.
 *
 * Available variables:
 * - $author: Comment author. Can be link or plain text.
 * - $content: Body of the post.
 * - $date: Date and time of posting.
 * - $links: Various operational links.
 * - $new: New comment marker.
 * - $picture: Authors picture.
 * - $signature: Authors signature.
 * - $status: Comment status. Possible values are:
 *   comment-unpublished, comment-published or comment-preview.
 * - $submitted: By line with date and time.
 * - $title: Linked title.
 *
 * These two variables are provided for context.
 * - $comment: Full comment object.
 * - $node: Node object the comments are attached to.
 *
 * @see template_preprocess_comment()
 * @see theme_comment()
 */
?>
<div class="comment <?print $status ?> clear-block">
  <?php if ($navlinks) { print $navlinks; } ?>
  <?php if ($links) { print $links; } ?>

  <br>
  <?php print $comment->picture ?>

  <div>
  <strong>Name:</strong> <?php print $comment->name . ' (<a href="http://tools.whois.net/?fuseaction=ipaddress.results&submit=Search&host=' . $comment->hostname . '">' . $comment->hostname . '</a>)' ?><br>

  <?php if ($comment->mail): ?>
  <strong>Email:</strong> <?php print $email ?><br>
  <?php endif; ?>
  <?php if ($comment->homepage): ?>
  <strong>Homepage:</strong> <?php print $homepage ?><br>
  <?php endif; ?>
  <strong>Date:</strong> <?php print $date ?><br>
  </div>

  <h3><?php print $title ?></h3>
  <div class="content">
    <?php print $comment->comment ?>
    <?php if ($comment->signature): ?>
    <div class="user-signature clear-block">
      <?php print $comment->signature ?>
    </div>
    <?php endif; ?>
  </div>

</div>
